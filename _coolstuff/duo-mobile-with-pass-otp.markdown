---
title: "Ditch Duo Mobile for pass-otp"
desc: "Add the flexibility of Linux to your 2FA"
sortable: 4
---

My university uses Duo Mobile for 2FA, which sucks because Duo is iOS and Android only. However, it isn't _too_ hard to import everything into password_store. To start, install the [pass](https://www.passwordstore.org/) utility for your Linux distro, and also install the [pass-otp](https://github.com/tadfisher/pass-otp) extension. A guide for getting `pass` configured is available [here](passwords-with-password-store.html).

The Duo Mobile app has two authentication mechanisms, one of which is a one-time-password (OTP) generator. This OTP generator is our target. In order to access the secret used by Duo, you'll have to either root your Android phone or install Duo in an emulator such as [anbox](https://anbox.io/). I installed anbox on my Arch Linux machine using [this](https://forum.manjaro.org/t/running-android-applications-on-arch-using-anbox/53332/9) tutorial, then installed the Duo Mobile APK and registered with my university.

Once installed and registered, take a look at the file `/var/lib/anbox/data/data/com.duosecurity.duomobile/files/duokit/accounts.json`, in which you should find a stanza of json that looks somewhat like the following:

```
"otpGenerator": {
  "otpSecret": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\u003d\u003d\u003d\u003d",
  "counter": 4
}
```

(I X'd out my secret key and set the counter value to a [guaranteed random number](https://www.xkcd.com/221/)). The \u003d characters are for padding and can be ignored.

Using the above information, add the secret key to `pass` as follows:

```
$ pass otp insert -e myuniversity/myid-otp
Enter otpauth:// URI for myuniversity/myid-otp: otpauth://hotp/myuniversity?secret=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&counter=4
```

Now you can obtain OTP tokens from the command line!

```
$ pass otp myuniversity/myid-otp
123456
```

If you use this OTP generator on multiple devices make sure that the counters stay in sync, otherwise the tokens won't be accepted because the service you are authenticating against will detect repeated tokens. The recommended way to accomplish this is to use the built-in git functionality in the `pass` utility.

## Google Authenticator: Easy!

If your university provides an option to set up OTP with Google Authenticator, then you may find that it is comparatively much easier to set up. When setting up Google Authenticator for your university, rather than scanning the barcode, click the "Can't scan the QR Code?" button to obtain the 16-character secret key. Then, insert the key into `pass` as follows:

```
$ pass otp insert -e myuniversity/myid-otp
Enter otpauth:// URI for myuniversity/myid-otp: otpauth://totp/myuniversity?secret=XXXXXXXXXXXXXXXX
```

where the X'd out key is the secret key that would normally be provided to Google Authenticator. Note that this setup uses totp (time-based) rather than hotp (counter-based). As such there isn't a counter to keep synchronized between devices, but it does become necessary to keep your system clock synchronized using NTP.
