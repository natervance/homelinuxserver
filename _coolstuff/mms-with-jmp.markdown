---
title: "Swap Google Voice for jmp.chat"
desc: "Use open source software for your MMS bridge"
sortable: 5
---

[jmp.chat](https://jmp.chat) provides a SMS/MMS to XMPP bridging service costing (as of summer 2019) $2.99 a month for unlimited texting and 120 minutes of voice calls. Behind the scenes, it (as of summer 2019) utilizes [bandwidth.com](https://www.bandwidth.com) as a backend, and integrates with a user-specified XMPP server to deliver text messages. It also provides a SIP address for voice calls. In general, it shouldn't be _too_ difficult to replicate this kind of service:
 1. Sign up for an account at a VoIP/SMS/MMS provider such as Bandwidth or Twilio.
 2. Set up a SIP server (such as Asterisk) and an XMPP server (such as Prosody).
 3. Write a bit of code to bridge the service provider's API to the SIP and XMPP servers.

Since I am a lazy lazy person (and also since I wanted at least one communication service that I don't self-host) I went with jmp.chat rather than attempt to re-invent it myself. Here are a few of my observations:
 * jmp.chat recommends dismail.de as the XMPP server, and I can confirm that it works quite well. This [compliance ranking](https://compliance.conversations.im/) shows the capabilities of many servers.
 * The SIP integration in Android's default calling app (at least as far as Lineage OS goes) works with the SIP server no problem.
 * The Conversations app on Android (from f-droid) works well, but Conversations with IPv6 support does not.
 * On the Linux desktop, Linphone works well for calls and Gajim is great for XMPP. If the default Gajim multi-window layout offends your sensibilities, do not fear: it is highly configurable and can display a single window with the roster as a side-panel (like Dino, but more features and fewer bugs).
 * For dismail.de and 5222.de (and perhaps others that I did not test), the default server-side settings for handling messages that have been delivered to a client is to discard them. This is fine if you only plan to access your messages from a single client, but if you want to have a message history that is available from every client, then in Gajim -> Advanced -> Archiving Preferences, set to Always and save (this configures the server to retain messages and does not affect the client). Don't fret if Gajim complains that the server did not send a response; only fret if after performing this operation you receive a message in one client, but then upon launching another client it is not available.
