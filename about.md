---
layout: page
title: About
permalink: /about/
---

# This Site
This server, vance.homelinuxserver.org, runs Debian 10 and was configured entirely using free and open source software. It is part of a larger set of free and open source technologies that makes it possible for me, the primary user of this server, to cut ties with large tech companies like Google. The goal of this site is to document the process of setting up this server so that others can do the same.

The scope of this server covers the following:
 * [Postfix](/serverdocs/email-with-postfix.html) to replace Gmail.
 * [Nextcloud](/serverdocs/file-sharing-with-nextcloud.html) to replace Google Drive.
   * Pair it with [LibreOffice](https://www.libreoffice.org/) to replace Google Docs, Sheets, and Slides.
   * Install [a plugin](https://apps.nextcloud.com/apps/calendar) to replace Google Calendar.
   * [Another plugin](https://apps.nextcloud.com/apps/mail) replaces the Gmail web client.
   * And [another](https://apps.nextcloud.com/apps/spreed) does video calls.
   * You can even [stream music](https://apps.nextcloud.com/apps/music), thus replacing Spotify and Pandora.
 * [Searx](/server/proxy-search-with-searx.html) doesn't exactly replace Google Search, but it does give the user more control.

While the above can easily be hosted from home, there are many additional services for which it is extremely difficult. I can recommend a few open source and/or freedom loving alternatives:
 * [BitChute](https://www.bitchute.com/) to replace YouTube.
   * Pro: It's web-torrent based and operates in your browser, which is awesome!
   * Con: It's definitely an alternative, with the content limitations that come with it.
 * [LBRY](https://lbry.com/) is another approach to YouTube replacement.
    * Pro: It's fully decentralized and has a built in producer payment system using cryptocurrencies. It can be run with a client or in the web browser.
    * Con: As with BitChute, it is severely limited in content.
 * [Mastodon](https://mastodon.social/about) to replace social media.
 * [JMP](/coolstuff/mms-with-jmp.html) can replace Google Voice.
 * [Librem 5](https://puri.sm/shop/librem-5/) replaces Android and iOS. If you don't want the hardware investment, I've been hanging out with a de-googled Lineage OS for some time now, and can say that it gets the job done nicely. I have also heard wonderful things about [PinePhone](https://www.pine64.org/pinephone/), though I can't speak from personal experience.
