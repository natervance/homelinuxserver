---
title: "Proxy Search with searx"
desc: "Self hosted search aggregator: because I don't trust any of them."
sortable: 7
---

[Searx](https://github.com/asciimoo/searx) is an open source "metasearch engine", meaning that it proxies searches to a configurable list of search engines and aggregates the results. The primary purpose is to remove or dilute the bias which search engines like Google add to the results. Additionally, searx provides some privacy by not giving search engines cookies, telling them which link was clicked, or revealing browser information that would be harvested if the engine was visited directly. Searx can optionally be configured to provide strong anonymity through TOR.

Searx is installed as follows (instructions modified from [this](https://asciimoo.github.io/searx/dev/install/installation.html) tutorial):
1. Install required packages:
   ```
   # apt install git build-essential libxslt-dev python-dev python-virtualenv python-babel zlib1g-dev libffi-dev libssl-dev uwsgi uwsgi-plugin-python
   ```
2. Install searx in a python virtualenv:
   ```
   # cd /usr/local
   # git clone https://github.com/asciimoo/searx.git
   # useradd searx -d /usr/local/searx
   # chown searx:searx -R /usr/local/searx
   # su - searx
   $ cd /usr/local/searx
   $ virtualenv searx-ve
   $ . ./searx-ve/bin/activate
   $ ./manage.sh update_packages
   $ exit
   ```
3. Set the secret key in searx settings:
   ```
   # cd /usr/local/searx
   # sed -i -e "s/ultrasecretkey/`openssl rand -hex 16`/g" searx/settings.yml
   ```
4. Set the base url in searx settings `/usr/local/searx/searx/settings.yml`:
   ```
   base_url : http://subdomain.domain.com/searx/
   ```
5. Configure uwsgi to run searx by creating the file `/etc/uwsgi/apps-available/searx.ini` containing the following:
   ```
   [uwsgi]
   # Who will run the code
   uid = searx
   gid = searx
   
   # disable logging for privacy
   disable-logging = true
   
   # Number of workers (usually CPU count)
   workers = 4
   
   # The right granted on the created socket
   chmod-socket = 666
   
   # Plugin to use and interpretor config
   single-interpreter = true
   master = true
   plugin = python
   lazy-apps = true
   enable-threads = true
   
   # Module to import
   module = searx.webapp
   
   # Virtualenv and python path
   virtualenv = /usr/local/searx/searx-ve/
   pythonpath = /usr/local/searx/
   chdir = /usr/local/searx/searx/
   ```
6. Activate searx in uwsgi:
   ```
   # cd /etc/uwsgi/apps-enabled
   # ln -s ../apps-available/searx.ini
   # systemctl restart uwsgi.service
   ```
7. Configure searx as a subdirectory in nginx by creating the file `/etc/nginx/conf.d/searx.conf` with the following:
   ```
   location = /searx { rewrite ^ /searx/; }
   location /searx {
           try_files $uri @searx;
   }
   location @searx {
           uwsgi_param SCRIPT_NAME /searx;
           include uwsgi_params;
           uwsgi_modifier1 30;
           uwsgi_pass unix:/run/uwsgi/app/searx/socket;
           access_log /dev/null;
           error_log /dev/null;
   }
   ```
   This configuration assumes the nginx setup from configuring [nextcloud](file-sharing-with-nextcloud.html).
8. Restart nginx:
   ```
   # systemctl restart nginx.service
   ```

If you are concerned about anonymity, please realize that a private searx instance as configured above will more than likely be insufficient. In the above configuration, anonymity can only be possible if many users search using the same searx instance; this is analogous to anonymity via a VPN where many users tunnel their traffic through the same host. Unfortunately, if you have set up your own private instance servicing only a few users, then the anonymity provided is extremely weak. In order to strengthen anonymity we configure searx to route all proxied searches through TOR.

1. Install TOR:
   ```
   # apt install tor
   ```
2. Configure searx to use TOR by adding the following to `/usr/local/searx/searx/settings.yml`:
   ```
   proxies :
       http : socks5://localhost:9050
       https: socks5://localhost:9050
   ```
3. Restart uwsgi:
   ```
   # systemctl restart uwsgi.service
   ```

Since searx accesses TOR's port from the local machine, then firewall modifications should not be necessary. If adding TOR integration introduces issues, make sure TOR's service is running without errors (it should be auto-started on installation):
```
systemctl status tor.service
```

Also, according to [this](https://github.com/asciimoo/searx/issues/886) thread, you may need to adjust the timeouts for the search engines to account for the increased latency of TOR, though I did not find this to be necessary.

Finally, remember that the IP address which the search engines see (and which we just anonymized via TOR) is only one of many variables that companies like Google consider when revealing your identity:

 * If you use the "autocomplete" feature in the searx preferences to suggest searches as you type, then be aware that the timing of your keystrokes, though partially masked by searx, can be used to fingerprint you. When selecting an engine to provide this feature use one that does not keep logs (e.g., DuckDuckGo) rather than Google.
 * The phrasing of your searches can be used to fingerprint you with a high degree of accuracy. [Here](https://www.kaggle.com/christopher22/stylometry-identify-authors-by-sentence-structure) is a guide for implementing a basic machine learning algorithm to accomplish this yourself. The authors of that article achieved 65% accuracy with limited resources; Google can do far better.
 * Searx's use of TOR can help to protect your anonymity while you perform the search (caveats apply - see previous points). But once you click a link in the results, your privacy is in your own hands. Be aware that unscrupulous sites can easily track you by leveraging the [features of your web browser](https://www.sciencedirect.com/science/article/pii/S1353485810700593?via%3Dihub). To defend against this, use an anonymity-focused web browser such as [tor browser](https://www.torproject.org/download/download-easy.html.en) and use an operating system that doesn't track you (most Linux distributions are safe, though [PureOS](https://www.pureos.net/), [Trisquel](https://trisquel.info/), [Qubes OS](https://www.qubes-os.org/), and [Tails](https://tails.boum.org/) have exceptionally good reputations regarding privacy).
 * Avoid signing in to services when possible. If you must sign in to, e.g., Google, then do so in a dedicated "insecure" browser. In that way you can compartmentalize your anonymous and nonymous browsing.
 * If you use software and services that advertise anonymity, remember that there is a difference between anonymity by policy and anonymity by design. VPNs, privacy-respecting proprietary software, and privacy-respecting web services like DuckDuckGo are examples of anonymity by policy: there is no technical barrier keeping your identity safe, only a promise. TOR, open source software, and self-hosted services like searx are examples of anonymity by design: the system itself ensures anonymity, and only by breaking or hacking the system can anonymity be compromised.
